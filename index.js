
const FIRST_NAME = "Andrei";
const LAST_NAME = "Bold";
const GRUPA = "1085";

/**
 * Make the implementation here
 */
class Employee {
    constructor(_name, _surname, _salary) {
        this.name = _name;
        this.surname = _surname;
        this.salary = _salary;
    }

    getDetails() {
        return this.name + ' ' + this.surname + ' ' + this.salary;
    }
}


class SoftwareEngineer extends Employee {
        constructor(_name, _surname, _salary, _experience){
            super(_name, _surname, _salary);
            if(_experience != null)
                this.experience = _experience;
                else {
                    this.experience = 'JUNIOR';
                }
        }
    

    applyBonus() {
        if(this.experience === 'JUNIOR' || (this.experience !== 'JUNIOR' && this.experience !=='MIDDLE' && this.experience !== 'SENIOR')) 
              return this.salary * 1.1;
        else 
        if(this.experience === 'MIDDLE' ) return this.salary * 1.15;
        else 
        if(this.experience === 'SENIOR') return this.salary * 1.20;
     
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

